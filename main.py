from flask import Flask
from flask import request, jsonify
from labyrinth.resolver import LabyrinthResolver
from labyrinth.exception import InvalidInputException, NoSolutionFoundException

app = Flask(__name__)


@app.route("/", methods=['GET', 'HEAD'])
def index():
    return "<h1>Labyrinth resolver</h1>POST data to resolve."


@app.route("/", methods=['POST'])
def solve():
    l_resolver = LabyrinthResolver(request.data)
    response = {'solution': None, 'length': None}
    try:
        r = l_resolver.resolve()
        response['solution'] = r
        response['length'] = len(r)
        response['status'] = 'OK'
    except InvalidInputException:
        response['error'] = 'Invalid input'
    except NoSolutionFoundException:
        response['status'] = 'No solution found'
    return jsonify(response)

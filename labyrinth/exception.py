class InvalidInputException(Exception):
    def __init__(self):
        super(InvalidInputException, self).__init__()


class NoSolutionFoundException(Exception):
    def __init__(self):
        super(NoSolutionFoundException, self).__init__()
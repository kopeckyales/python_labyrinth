import json
from dijkstar import Graph, find_path, NoPathError
from labyrinth.exception import InvalidInputException, NoSolutionFoundException


class LabyrinthResolver:
    def __init__(self, user_input):
        self.user_data = json.loads(user_input)

    def resolve(self):
        self.validate_data()
        return self.get_solution()

    def validate_data(self):
        if self.user_data['start'] not in self.user_data['rooms'] or \
                self.user_data['end'] not in self.user_data['rooms'] or \
                len(self.user_data['corridors']) == 0:
            raise InvalidInputException
        for edge in self.user_data['corridors']:
            if len(edge) != 2 or edge[0] == edge[1] or \
                    edge[0] not in self.user_data['rooms'] or edge[1] not in self.user_data['rooms']:
                raise InvalidInputException

    def get_solution(self):
        graph = Graph()
        for val in self.user_data['corridors']:
            graph.add_edge(val[0], val[1], {'cost': 1})
            graph.add_edge(val[1], val[0], {'cost': 1})

        try:
            result = find_path(graph, self.user_data['start'], self.user_data['end'],
                               cost_func=lambda u, v, e, prev_e: e['cost'])
        except NoPathError:
            raise NoSolutionFoundException
        return result.nodes
